package testers;

import characters.Elf;
import characters.MyCharacter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ElfTest {
    private final MyCharacter CHARACTER = new Elf();

    @Test
    void getCharisma() {
        assertEquals(10, CHARACTER.getCharisma());
    }

    @Test
    void getConstitution() {
        assertEquals(10, CHARACTER.getConstitution());
    }

    @Test
    void getDexterity() {
        assertEquals(12, CHARACTER.getDexterity());
    }

    @Test
    void getIntelligence() {
        assertEquals(10, CHARACTER.getIntelligence());
    }

    @Test
    void getName() {
        assertEquals("noname", CHARACTER.getName());
    }

    @Test
    void getSkills() {
        assertEquals("Darkvision, Keen Senses, Fey Ancestry, Trance", CHARACTER.getSkills());
    }

    @Test
    void getStrength() {
        assertEquals(10, CHARACTER.getStrength());
    }

    @Test
    void getWisdom() {
        assertEquals(10, CHARACTER.getWisdom());
    }

    @Test
    void getHasItem() {
        assertTrue(CHARACTER.getHasItem());
    }

    @Test
    void setCharisma() {
        CHARACTER.setCharisma(20);
        assertEquals(20, CHARACTER.getCharisma());
    }

    @Test
    void setConstitution() {
        CHARACTER.setConstitution(20);
        assertEquals(20, CHARACTER.getConstitution());
    }

    @Test
    void setDexterity() {
        CHARACTER.setDexterity(20);
        assertEquals(20, CHARACTER.getDexterity());
    }

    @Test
    void setIntelligence() {
        CHARACTER.setIntelligence(20);
        assertEquals(20, CHARACTER.getIntelligence());
    }

    @Test
    void setStrength() {
        CHARACTER.setStrength(20);
        assertEquals(20, CHARACTER.getStrength());
    }

    @Test
    void setWisdom() {
        CHARACTER.setWisdom(20);
        assertEquals(20, CHARACTER.getWisdom());
    }

    @Test
    void setName() {
        CHARACTER.setName("Greg");
        assertEquals("Greg", CHARACTER.getName());
    }

    @Test
    void setSkills() {
        CHARACTER.setSkills("Many Skills");
        assertEquals("Many Skills", CHARACTER.getSkills());
    }

    @Test
    void setHasItem() {
        CHARACTER.setHasItem(true);
        assertTrue(CHARACTER.getHasItem());
    }

    @Test
    void testToString() {
        String characterString;
        characterString = String.format("%s\n%d\n%d\n%d\n%d\n%d\n%d\n%s\n%b\n",
                CHARACTER.getName(),CHARACTER.getStrength(),CHARACTER.getDexterity(),
                CHARACTER.getConstitution(),CHARACTER.getIntelligence(),CHARACTER.getWisdom(),
                CHARACTER.getCharisma(),CHARACTER.getSkills(),CHARACTER.getHasItem());
        assertEquals(characterString, CHARACTER.toString());
    }

    @Test
    void toLongString() {
        String characterString;
        characterString = String.format("These are your characters attributes:\n" +
                        "Type: %s\n" + "Strength: %d\n" + "Dexterity: %d\n" + "Constitution: %d\n" + "Intelligence: %d\n"+
                        "Wisdom: %d\n"+ "Charisma: %d\n"+ "Name: %s\n"+ "Skills: %s\n"+ "Weapon: %s\n",
                CHARACTER.getClass().getSimpleName(),CHARACTER.getStrength(),CHARACTER.getDexterity(),
                CHARACTER.getConstitution(),CHARACTER.getIntelligence(),CHARACTER.getWisdom(),
                CHARACTER.getCharisma(),CHARACTER.getName(),CHARACTER.getSkills(),CHARACTER.getHasItem());
        assertEquals(characterString, CHARACTER.toLongString());
    }
}