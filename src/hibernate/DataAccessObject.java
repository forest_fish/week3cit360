package hibernate;

import characters.MyCharacter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.persistence.NoResultException;
import java.util.List;

public class DataAccessObject {

    SessionFactory factory;
    Session session = null;

    private static DataAccessObject single_instance = null;

    private DataAccessObject() { factory = FactoryMaker.getSessionFactory(); }

    public static DataAccessObject getInstance() {

        if (single_instance == null) {
            single_instance = new DataAccessObject();
        }

        return single_instance;
    }

    //Retrieves all entries in a table
    public List<MyCharacter> getCharacters() {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from characters.MyCharacter";
            List<MyCharacter> characterList = (List<MyCharacter>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return characterList;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    //Gets a single entry from the table based on id
    public MyCharacter getCharacter (String name) throws NoResultException {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from characters.MyCharacter where charname= \'" + (String) name + "\'";
            MyCharacter c = (MyCharacter) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;
        }catch (NoResultException nre) {
            session.getTransaction().rollback();
            throw nre;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    //Adds and entry the the table
    public Integer addCharacter(MyCharacter userCharacter){
        Integer id = null;

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(userCharacter);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session.getTransaction()!=null) session.getTransaction().rollback();
        } finally {
            session.close();
        }
        return id;
    }

    public Integer changeCharacter  (MyCharacter userCharacter){
        Integer id = null;

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.update(userCharacter);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session.getTransaction()!=null) session.getTransaction().rollback();
        } finally {
            session.close();
        }
        return id;
    }

    //deletes an entry from the table based on id
    /*public void deleteHero(Integer id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Hero hero = (Hero)session.get(Hero.class, id);
            session.delete(hero);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session.getTransaction() != null) session.getTransaction().rollback();
        }
    }*/
}
