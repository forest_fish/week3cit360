package hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

//Builds a session factory and sends and returns it
public class FactoryMaker {

    public static SessionFactory getSessionFactory() { return sessionFactory; }

    private static final SessionFactory sessionFactory = sessionFactoryBuilder();

    private static SessionFactory sessionFactoryBuilder() {
        try {
            ServiceRegistry serviceRegistry = new //
                    StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

            Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

            return metadata.getSessionFactoryBuilder().build();
        } catch (Throwable ex) {

            System.err.println("Error creating the SessionFactory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}
