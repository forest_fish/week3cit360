package characters;

import hibernate.DataAccessObject;

import javax.persistence.NoResultException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CharacterMaker {
    /**This is an often used string used in error handling */
    static String badInput = "Invalid input. You must enter a \"y\" or a \"n\". Type \"end\" to quit.";
    static DataAccessObject dao = DataAccessObject.getInstance();
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        MyCharacter userCharacter;
        String answeString;
        String answeString2;

        System.out.printf("Welcome the character builder.\n"+
            "Please answer all questions in the formats that are shown in the parenthesis.\n"+
            "Do you want to load a character that was previously made? (y or n): "
        );

        /**
         * This loop runs the decision making to determine if we are creating a character or loading a pre made character.
         * It also provides input error handling and will run until a correct input is entered.
         * if the user answers yes it runs the loadCharacter method and if they answer no it runs the buildCharacter method.
         * Entering end will run the endProgram method.
         */
        while (true) {
            answeString = in.nextLine();

            if (answeString.equalsIgnoreCase("y")) {
                userCharacter = loadCharacter(in);
                break;

            } else if (answeString.equalsIgnoreCase("n")) {
                userCharacter = buildCharacter(in);
                break;

            } else if (answeString.equalsIgnoreCase("end")) {
                endProgram();

            } else {
                /** If no good input is found it prints out the badInput string */
                System.out.println(badInput);
                in.nextLine();
            }          
        }

        /**
         * Displays the current character attributes after running either the loadCharacter method or the buildCharacter methods.
         * Allows to user to review and determine if any changes need to be made.
         */
        System.out.println(userCharacter.toLongString());
        
        /**
         * Asks if any changes need to be made. if yes it runs the changeCharacter method. If no it asks if they would like to save.
         * If they answer yes it runs the saveCharacter method. If they answer no it tells them how to quit the program. It loops
         * back and allows the user to make any changes that they thought of. It will redo all these steps until the user types end.
         * Entering end will run the endProgram method.
         * There is error handling built into the loop.
         */
        do {
            System.out.printf("Are there any changes that you would like to make to the character? (y or n)\nTo quit type \"end\"\n");
            answeString2 = in.nextLine();
            
            if (answeString2.equalsIgnoreCase("y")){
                userCharacter = changeCharacter(in, userCharacter);

            } else if (answeString2.equalsIgnoreCase("n")) {
                System.out.println("Would you like to save your character? (y or n)");
                answeString2 = in.nextLine();
                
                if(answeString2.equalsIgnoreCase("y")) {
                    saveCharacter(in, userCharacter);
                    
                } else if (answeString2.equalsIgnoreCase("n")) {
                    System.out.println("To quit the program type \"end\".");

                } else if (answeString2.equalsIgnoreCase("end")) {
                    endProgram();
    
                } else {
                    System.out.println(badInput);
                }

            } else if (answeString2.equalsIgnoreCase("end")) {
                endProgram();

            } else {
                System.out.println(badInput);
            }

        } while (!answeString2.equalsIgnoreCase("end"));

        in.close();
    }

     /**
      * This method is used to load and generate a character based on a save text file. Returns the character to be edited by the user or resaved.
      * @param in
      * @return Character
      */
    public static MyCharacter loadCharacter(Scanner in) {
        MyCharacter loadedCharacter = null;
        System.out.println("running loadCharacter");
        System.out.println("Do you know the name of the character? (y or n)");
        String ans = in.nextLine();
        do {
            if (ans.equalsIgnoreCase("y")) {
                loadedCharacter = chooseCharacter(in);
                break;
            } else if (ans.equalsIgnoreCase("n")) {
                List<MyCharacter> characters = dao.getCharacters();
                for (MyCharacter i : characters) { System.out.println(i.toLongString()); }
                loadedCharacter = chooseCharacter(in);
                break;
            } else if (ans.equalsIgnoreCase("end")) {
                endProgram();
            } else {
                System.out.println(badInput);
            }
        } while (!ans.equalsIgnoreCase("end"));

        /*boolean loaded = false;
        int tries = 0;
        do{
            /**
             * This try block finds the file that the user inputs, reads the data from the file, saves it in the appropriate variables, and passes it to the right constructor.
             * It uses some math to avoid altering the attributes due to the constructors creation process.
             *
            try {
                String fileName = in.nextLine();
                File loadFile = new File(fileName);
                ObjectMapper mapper = new ObjectMapper();
                Scanner loader = new Scanner(loadFile);
                String jsonString = loader.nextLine();

                if (jsonString.contains("\"type\":1")) {
                    /** Builds Elf characters *
                    loadedCharacter = mapper.readValue(jsonString, Elf.class);
                    loaded = true;

                } else if (jsonString.contains("\"type\":2")) {
                    /** Builds Dwarf characters *
                    loadedCharacter = mapper.readValue(jsonString, Dwarf.class);
                    loaded = true;

                } else if (jsonString.contains("\"type\":3")) {
                    /** Builds Human characters *
                    loadedCharacter = mapper.readValue(jsonString, Human.class);
                    loaded = true;

                } else if (jsonString.contains("\"type\":4")) {
                    /** Builds typeless characters *
                    loadedCharacter = mapper.readValue(jsonString, MyCharacter.class);
                    loaded = true;

                }
                loader.close();
            } catch (FileNotFoundException e) {
                /**
                 * Catches errors in the file name that was entered by the user. If there are 10 failed attempts the program will exit.
                 *
                System.out.println("The file could not be found. Please reenter the file name.");
                System.out.println("The program will exit after " + (10 - tries) + " more attempts.");
                if (tries > 9) {
                    endProgram();
                }
                tries ++;
            } catch (JsonProcessingException e) {
                System.err.println(e.toString());
            }

        } while (!loaded);
        */
        return loadedCharacter;
    }

    private static MyCharacter chooseCharacter(Scanner in) {
        MyCharacter loadedCharacter = null;
        System.out.println("Enter the name of the character you want to load. (Case insensitive)");
        boolean found = true;
        do {
            try {
                loadedCharacter = dao.getCharacter(in.nextLine());
                found = false;
            } catch (NoResultException e) {
                System.out.println("The name doesn't exist. Enter a Character from the list.");
            }
        } while (found);
        return loadedCharacter;
    }

    /**
     * This method takes the character that was created or edited by the user and saves its info in a text file with a name the user defines.
     * @param in
     * @param userCharacter
     */
    public static void saveCharacter(Scanner in, MyCharacter userCharacter) {
        if (userCharacter.getId() == 0) {
            dao.addCharacter(userCharacter);
        } else {
            dao.changeCharacter(userCharacter);
        }
        System.out.println("Character Saved");

        /*System.out.println("Enter the name for the file that you want to save the character in. (must end in \".txt\")\n"+
            "ex: elf.txt, mycharacter.txt, jeff.txt");
        String fileName = in.nextLine();
        ObjectMapper mapper = new ObjectMapper();
        String jsonData = "";
        try {
            File saveFile = new File(fileName);
            if (saveFile.exists()) {
                boolean finish = false;
                System.out.println("This save file already exists. Do you want to override it? (y or n)");
                do {
                    String overwrite = in.nextLine();
                    if (overwrite.equalsIgnoreCase("y")) {
                        mapper.writeValue(saveFile, userCharacter);
                        System.out.println("Saving Character");
                        finish = true;

                    } else if (overwrite.equalsIgnoreCase("n")) {
                        System.out.print("Character not saved.");
                        finish = true;

                    } else {
                        System.out.println("You must enter a \"y\" or a \"n\"");

                    }
                } while (!finish);
            } else {
                mapper.writeValue(saveFile, userCharacter);
                System.out.println("Saving Character");

            }
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("There was an issue while saving the program. \nPlease try again. \nIf it continues to fail please quit the program and run it again.");
        }*/
    }

    /**
     * This program asks the user what type of character they want to create. It then asks if they want it to have the default characteristic.
     * If they choose no it goes through each attribute and then creates a character with the selected parameters.
     * @param in
     * @return Character
     */
    public static MyCharacter buildCharacter(Scanner in) {
        MyCharacter builtCharacter = null;

        int strength, dexterity, constitution, intelligence, wisdom, charisma, type = 0;
        @SuppressWarnings ("unused")
        String name, skills;
        boolean generic = true;
        Boolean hasItem = null;
        String answeString;
        
        System.out.printf("Please choose your character type.\n1. Elf\n2. Dwarf\n3. Human\n4. No Type\n");
        do {
            try {
                type = in.nextInt();

                if (!(type == 1 || type == 2 || type == 3 || type == 4)) {
                    System.out.println("The value must be either a 1, 2, 3, or 4. Please enter one of these values.");
                }
                in.nextLine();

            } catch (InputMismatchException e) {
                System.out.println("The value must be a numerical character. It must be either a 1, 2, 3, or 4. Please enter one of these values.");
                in.nextLine();
            }

        } while(!(type == 1 || type == 2 || type == 3 || type == 4));


        System.out.println("Do you want to make a custom Character? (y or n)");
        do {
            answeString = in.nextLine();

            if (answeString.equalsIgnoreCase("y")) {
                generic = false;
                break;

            } else if (answeString.equalsIgnoreCase("n")) {
                generic = true;
                break;

            } else if (answeString.equalsIgnoreCase("end")){
                endProgram();

            }
            System.out.println(badInput);

        } while (!answeString.equalsIgnoreCase("y") || !answeString.equalsIgnoreCase("n"));

        if (generic) { 
            if (type == 1){
                builtCharacter = new Elf();
            } else if (type == 2) {
                builtCharacter = new Dwarf();
            } else if (type == 3) {
                builtCharacter = new Human();
            } else {
                builtCharacter = new MyCharacter();
            }
        } else {
            do {
                try {   
                    System.out.println("Enter the characters name");
                    name = in.nextLine();

                    System.out.println("Enter an integer value (a whole number) for strength:");
                    strength = in.nextInt();

                    System.out.println("Enter an integer value (a whole number) for dexterity:");
                    dexterity = in.nextInt(); 

                    System.out.println("Enter an integer value (a whole number) for constitution:");
                    constitution = in.nextInt();

                    System.out.println("Enter an integer value (a whole number) for intelligence:");
                    intelligence = in.nextInt();

                    System.out.println("Enter an integer value (a whole number) for wisdom:");
                    wisdom = in.nextInt(); 

                    System.out.println("Enter an integer value (a whole number) for charisma:");
                    charisma = in.nextInt();

                    /**
                     * Clears the input stream
                     */
                    in.nextLine();

                    if (type == 4) {
                        builtCharacter = new MyCharacter(strength, dexterity, constitution, intelligence, wisdom, charisma, name);
                    } else if (type == 1) {
                        System.out.println("Does your character have a bow (y or n):");
                        while (hasItem == null) {
                            String bow = in.nextLine();
                            if (bow.equalsIgnoreCase("y")) {
                                hasItem = true;
                            } else if (bow.equalsIgnoreCase("n")) {
                                hasItem = false;
                            } else {
                                System.out.println("Enter a \"y\" or a \"n\":");
                            }
                        }
                        builtCharacter = new Elf(strength, dexterity, constitution, intelligence, wisdom, charisma, name, hasItem);

                    } else if (type == 2) {
                        System.out.println("Does your character have a axe (y or n):");
                        while (hasItem == null) {
                            String axe = in.nextLine();
                            if (axe.equalsIgnoreCase("y")) {
                                hasItem = true;
                            } else if (axe.equalsIgnoreCase("n")) {
                                hasItem = false;
                            } else {
                                System.out.println("Enter a \"y\" or a \"n\":");
                            }
                        }
                        builtCharacter = new Dwarf(strength, dexterity, constitution, intelligence, wisdom, charisma, name, hasItem);

                    } else if (type == 3) {
                        System.out.println("Does your character have a sword (y or n):");
                        while (hasItem == null) {
                            String sword = in.nextLine();
                            if (sword.equalsIgnoreCase("y")) {
                                hasItem = true;
                            } else if (sword.equalsIgnoreCase("n")) {
                                hasItem = false;
                            } else {
                                System.out.println("Enter a \"y\" or a \"n\":");
                            }
                        }
                        builtCharacter = new Human(strength, dexterity, constitution, intelligence, wisdom, charisma, name, hasItem);

                    }

                } catch (InputMismatchException e) {
                    System.out.println("You must enter the correct values.");
                    in.nextLine();
                } catch (NoSuchElementException e) {
                    System.out.println("You must enter a name for your character");
                } 
            } while (builtCharacter == null);
        }

        return builtCharacter;
    }
    
    /**
     * This method gets the attribute that the user wants to change and saves that option. It changes that option and returns the character.
     * @param in
     * @param userCharacter
     * @return
     */
    public static MyCharacter changeCharacter(Scanner in, MyCharacter userCharacter) {
        System.out.println(
            "Would you like to change:\n"+
            "1: Strength Attribute\n"+
            "2: Dexterity Attribute\n"+
            "3: Constitution Attribute\n"+
            "4: Intelligence Attribute\n"+
            "5: Wisdom Attribute\n"+
            "6: Charisma Attribute\n"+
            "7: Name\n"+
            "8: Skills\n"+
            "9: Holding a Weapon"
        );
        /** The option is declared outside of the loop so that the selection is saved if there is an issue with the value the user enters. */
        int changeItem = 0;
        do{
            try {
                changeItem = in.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("You must enter the number of the item you wish to change.");
                in.nextLine();
            }
            if (changeItem < 1 || changeItem > 9) {
                System.out.println("That is not one of the options. Please choose again.");
            }
        } while (changeItem < 1 || changeItem > 9);
        
        boolean finish = false;
        do{
            try{
                if (changeItem == 1) {
                    System.out.println("Enter the new value for Strength (whole numbers):");
                    int strength = in.nextInt();
                    userCharacter.setStrength(strength);
                    finish = true;
                    in.nextLine();
                } else if (changeItem == 2) {
                    System.out.println("Enter the new value for Dexterity (whole numbers):");
                    int dexterity = in.nextInt();
                    userCharacter.setDexterity(dexterity);
                    finish = true;
                    in.nextLine();
                } else if (changeItem == 3) {
                    System.out.println("Enter the new value for Constitution (whole numbers):");
                    int constitution = in.nextInt();
                    userCharacter.setConstitution(constitution);
                    finish = true;
                    in.nextLine();
                } else if (changeItem == 4) {
                    System.out.println("Enter the new value for Intelligence (whole numbers):");
                    int intelligence = in.nextInt();
                    userCharacter.setIntelligence(intelligence);
                    finish = true;
                    in.nextLine();
                } else if (changeItem == 5) {
                    System.out.println("Enter the new value for Wisdom (whole numbers):");
                    int wisdom = in.nextInt();
                    userCharacter.setWisdom(wisdom);
                    finish = true;
                    in.nextLine();
                } else if (changeItem == 6) {
                    System.out.println("Enter the new value for Charisma (whole numbers):");
                    int charisma = in.nextInt();
                    userCharacter.setCharisma(charisma);
                    finish = true;
                    in.nextLine();
                } else if (changeItem == 7) {
                    System.out.println("Enter a new name:");
                    in.nextLine();
                    String name = in.nextLine();
                    userCharacter.setName(name);
                    finish = true;
                } else if (changeItem == 8) {
                    System.out.println("Enter new skills on one line seperated by a comma:");
                    in.nextLine();
                    String skills = in.nextLine();
                    userCharacter.setSkills(skills);
                    finish = true;
                } else if (changeItem == 9) {
                    System.out.println("Does your character have a weapon (y or n):");
                    in.nextLine();
                    String answer = in.nextLine();
                    if (answer.equalsIgnoreCase("y")) {
                        userCharacter.setHasItem(true);
                        finish = true;
                    } else if (answer.equalsIgnoreCase("n")) {
                        userCharacter.setHasItem(false);
                        finish = true;
                    } else {
                        System.out.println("You must enter a either a \"y\" or a \"n\"");
                    }
                }
            } catch (InputMismatchException e) {
                System.out.println("You must enter a whole number.");
                in.nextLine();
            }
        } while (!finish);

        /** Displays the character with the changes that have been made. */
        System.out.println("Changed Character");
        System.out.println(userCharacter.toLongString());
        return userCharacter;
    }

    /**
     * This method displays a message when called and then exits the program.
     */
    public static void endProgram() {
        System.out.println("Have a nice day! :)");
        System.exit(0);
    }

    /**
     * This method is used to convert the boolean value that the Character object returns for hasItem and turns it
     * into the right string based off of the type of character
     * @param userCharacter
     * @return
     */
    /*public static String getWeapon(MyCharacter userCharacter) {
        String weapon;
        if (userCharacter.getType() == 1 && userCharacter.getHasItem()) {
            weapon = "Bow";
        } else if (userCharacter.getType() == 2 && userCharacter.getHasItem()) {
            weapon = "Axe";
        } else if (userCharacter.getType() == 3 && userCharacter.getHasItem()) {
            weapon = "Sword";
        } else if (userCharacter.getType() == 4 && userCharacter.getHasItem()) {
            weapon = "Weapon";
        } else {
            weapon = "none";
        }
        return weapon;
    }*/
}