package characters;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import javax.persistence.*;


@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS)
@Entity
@Table(name = "built_characters")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "chartype", discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue(value = "4")


public class MyCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "strength")
    protected int strength;
    @Column(name = "dexterity")
    protected int dexterity;
    @Column(name = "constitution")
    protected int constitution;
    @Column(name = "intelligence")
    protected int intelligence;
    @Column(name = "wisdom")
    protected int wisdom;
    @Column(name = "charisma")
    protected int charisma;
    @Column(name = "skills")
    protected String skills;
    @Column(name = "charname")
    protected String name;
    @Column(name = "hasitem")
    protected boolean hasItem;

    public MyCharacter() {
        setStrength(10);
        setDexterity(10);
        setConstitution(10);
        setIntelligence(10);
        setWisdom(10);
        setCharisma(10);
        setSkills("noskills");
        setName("noname");
        setHasItem(false);
    }

    public MyCharacter(int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma, String name) {
        setStrength(strength);
        setDexterity(dexterity);
        setConstitution(constitution);
        setIntelligence(intelligence);
        setWisdom(wisdom);
        setCharisma(charisma);
        setSkills("noskills");
        setName(name);
        setHasItem(false);
    }

    /**
     * @return the charisma
     */
    public int getCharisma() {
        return charisma;
    }
    /**
     * @return the constitution
     */
    public int getConstitution() {
        return constitution;
    }
    /**
     * @return the dexterity
     */
    public int getDexterity() {
        return dexterity;
    }
    /**
     * @return the intelligence
     */
    public int getIntelligence() {
        return intelligence;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @return the skills
     */
    public String getSkills() {
        return skills;
    }
    /**
     * @return the strength
     */
    public int getStrength() {
        return strength;
    }
    /**
     * @return the wisdom
     */
    public int getWisdom() {
        return wisdom;
    }
    /**
     * @return the hasItem
     */
    public boolean getHasItem() {
        return hasItem;
    }

    /**
     * @param charisma the charisma to set
     */
    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }
    /**
     * @param constitution the constitution to set
     */
    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }
    /**
     * @param dexterity the dexterity to set
     */
    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }
    /**
     * @param intelligence the intelligence to set
     */
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
    /**
     * @param strength the strength to set
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }
    /**
     * @param wisdom the wisdom to set
     */
    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @param skills the skills to set
     */
    public void setSkills(String skills) {
        this.skills = skills;
    }
    /** 
     * @param hasItem the hasItem to set
     */
    public void setHasItem(boolean hasItem) {
        this.hasItem = hasItem;
    }

    @Override
    public String toString() {
        String characterString;
        characterString = String.format("%s\n%d\n%d\n%d\n%d\n%d\n%d\n%s\n%b\n",
                getName(),getStrength(),getDexterity(),getConstitution(),getIntelligence(),getWisdom(),getCharisma(),getSkills(),getHasItem()
            );
        return characterString;
    }
    public String toLongString() {
        String characterString;
        characterString = String.format("These are your characters attributes:\n" +
                "Type: %s\n" +
                "Strength: %d\n" +
                "Dexterity: %d\n" +
                "Constitution: %d\n" +
                "Intelligence: %d\n"+
                "Wisdom: %d\n"+
                "Charisma: %d\n"+
                "Name: %s\n"+
                "Skills: %s\n"+
                "Weapon: %s\n",
                getClass().getSimpleName(),getStrength(),getDexterity(),
                getConstitution(),getIntelligence(),getWisdom(),
                getCharisma(),getName(),getSkills(),getHasItem()
        );
        return characterString;
    }
    
    protected void classAtributes() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
