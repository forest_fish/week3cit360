package characters;

import javax.persistence.*;

@Entity
@DiscriminatorValue("3")
public class Human extends MyCharacter {

    public Human() {
        this.classAtributes();
        setHasItem(true);
    }
    public Human(int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma, String name, Boolean hasItem) {
        super(strength, dexterity, constitution, intelligence, wisdom, charisma, name);
        this.classAtributes();
        setHasItem(hasItem);
    }

    @Override
    protected void classAtributes() {
        setStrength(strength ++);
        setDexterity(dexterity ++);
        setConstitution(constitution ++);
        setIntelligence(intelligence ++);
        setWisdom(wisdom ++);
        setCharisma(charisma ++);
        setSkills("");
    }
}