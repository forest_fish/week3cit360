package characters;

import javax.persistence.*;

@Entity
@DiscriminatorValue("1")
public class Elf extends MyCharacter {

    public Elf() {
        this.classAtributes();
        setHasItem(true);
    }
    public Elf(int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma, String name, Boolean hasItem) {
        super(strength, dexterity, constitution, intelligence, wisdom, charisma, name);
        this.classAtributes();
        setHasItem(hasItem);
    }

    @Override
    protected void classAtributes() {
        setDexterity(dexterity + 2);
        setSkills("Darkvision, Keen Senses, Fey Ancestry, Trance");
    }
}