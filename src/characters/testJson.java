package characters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

public class testJson {
    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();
        MyCharacter character = new Elf();
        MyCharacter character1 = new Human();
        MyCharacter character2 = new Dwarf();
        MyCharacter character3 = new MyCharacter();
        List<MyCharacter> cList = new ArrayList<>();
        cList.add(character);
        cList.add(character1);
        cList.add(character2);
        cList.add(character3);
        for (MyCharacter c : cList) {
            try {
                String jsonS = mapper.writeValueAsString(c);
                System.out.println(jsonS);
                MyCharacter chari = mapper.readValue(jsonS, MyCharacter.class);
                System.out.println(chari.toLongString());
            } catch (JsonProcessingException e) {
                System.err.println(e.toString());
            }
        }
    }
}
