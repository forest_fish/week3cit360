package characters;

import javax.persistence.*;

@Entity
@DiscriminatorValue("2")
public class Dwarf extends MyCharacter {

    public Dwarf() {
        this.classAtributes();
        setHasItem(true);
    }
    public Dwarf(int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma, String name, Boolean hasItem) {
        super(strength, dexterity, constitution, intelligence, wisdom, charisma, name);
        this.classAtributes();
        setHasItem(hasItem);
    }

    @Override
    protected void classAtributes() {
        setConstitution(constitution + 2);
        setSkills("Darkvision, Dwarven Resliliance, Dwarven Combat Training, Tool Proficiency, Stonecunning");
    }
}