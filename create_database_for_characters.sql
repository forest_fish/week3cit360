create database Characters;
use Characters;

create table built_characters (
	id				int				not null 	auto_increment,
    charname		varchar(30),
    strength		int				not null,
    dexterity		int				not null,
    constitution 	int 			not null,
    intelligence	int				not null,
    wisdom			int				not null,
    charisma		int  			not null,
    hasitem			boolean			not null,
    chartype		int				not null,
    skills			varchar(200),
    primary key(id)    
)